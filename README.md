# Project-Aela
An in-house game engine developed by Ekkon Games. The engine currently supports the following features:
<ul>
    <li>2D and 3D rendering</li>
    <li>Resource loading and management</li>
    <li>3D Audio</li>
    <li>A UI system that supports buttons, text fields and other common elements</li>
    <li>More to come...</li>
</ul>

Collaborators: 3
